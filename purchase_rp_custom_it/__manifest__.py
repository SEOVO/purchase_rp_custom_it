# -*- encoding: utf-8 -*-
{
    'name': 'Reporte de Ordenes de compra para Semana Economica',
    'category': 'purchase',
    'author': 'ITGRUPO',
    'depends': ['purchase'],
    'version': '1.0',
    'description':"""
    Reporte de Ordenes de compra para Semana Economica
    """,
    'auto_install': False,
    'demo': [],
    'data': [
        'views/report_semana_economica.xml',

        ],
    'installable': True
}
